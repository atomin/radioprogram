require_relative '../config/environment'

module Schedule
  Options = Station.all.to_a | RadioProgram.all.to_a

  COMMANDS = ['set_heard', 'set_unheard', 'info']

  def self.launch(name, options)
    program = Options.find {|x| x.name == name}
    return not_found(name) if program.nil?

    if options.any? && COMMANDS.include?(options.first)
      launch_command(program, options.shift, options)
    else
      program.launch(options)
    end

  end

  def self.launch_command(program, command, options)
    puts program.send(command, options.first.try(:to_i) || 0)
    puts program.description if command != 'info'
  end

  def self.not_found(name)
    puts "\n"
    puts "program #{name} not found".red
    puts "\n"
    help
  end

  def self.help
    Options.each { |p|  puts p.description }
    puts 'Examples'
    puts 'To launch station culture:'
    puts '>> ' + 'program culture'.magenta
    puts
    puts 'To launch last past perfect radio program run command:'
    puts '>> ' + 'program pastperfect'.magenta
    puts 'To launch previous episode run command:'
    puts '>> ' + 'program pastperfect 1'.magenta
    puts 'command with 2 as second argument will run prev prev episode and so on'
    puts
    puts 'To set heard/unheard:'
    puts '>> ' + 'program exotica set_heard 0'.magenta
    puts '>> ' + 'program pastperfect set_unheard 1'.magenta
  end

  if ARGV.length == 0 || ARGV[0] =~ /^-h$/i || ARGV[0] =~ /help/i
    help
  else
    name = ARGV.shift.downcase
    options = ARGV
    launch(name, options)
  end
end
