class RadioProgram < ActiveRecord::Base
  belongs_to :station
  has_many :heard_programs

    def launch(options)
      weeks = options.first || 0
      date = get_program_date(weeks.to_i)

      launch_date(date)
      puts "from #{date.strftime('%d-%m-%Y').green} #{description}"
    end

    def launch_date(date)
      set_heard(date)
      station.launch_time(date, time)
    end

    def day_name
      Date::DAYNAMES[day_index].pluralize
    end

    def description(with_history = 5)
      str = "program #{name.humanize.ljust(13).green} on #{day_name.ljust(10).yellow} at #{time.yellow}"
      last_dates.first(with_history).each_with_index do |date, i|
        new_program = Date.today - date <= 2 
        color = heard?(date) ? 'blue' : 'green'
        str = str + "\n#{i} #{date.strftime('%d-%m-%Y').send( color )} #{'!'.green if new_program}"
      end

      str
    end

    def heard?(date)
      heard_programs.exists? date: date
    end

    def last_date
      date = Date.current.monday + (day-1).days
      date > Date.current ? date - 1.weeks : date 
    end 

    def last_dates
      Enumerator.new do |yielder, date = last_date|
       loop do 
        yielder.yield(date)
        date -= 1.weeks
        end
      end.lazy
    end

    def set_heard(date)
      date = get_program_date(date) if Integer === date
      unless heard?(date)
        heard_programs << HeardProgram.new(date: date)
        save
      end
    end

    def set_unheard(date)
      date = get_program_date(date) if Integer === date
      heard_programs.where(date: date).each { |h| h.destroy }
    end

    def info(with_history = 10)
      with_history = 10 if with_history == 0
      description(with_history)
    end

    def set_heard_last(n)
      last_dates.first(n).each { |date| set_heard date }
    end

    def day_index
      day == 7 ? 0 : day
    end

    def get_program_date(weeks_ago = 0)
      last_dates.drop(weeks_ago).first
    end
end
