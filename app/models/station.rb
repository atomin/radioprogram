class Station < ActiveRecord::Base
    def launch_current
      puts "station #{name.to_s.humanize.green}"

      Launchy.open("#{site_url}/stations/#{code}")
    end

    #time like '13:30'
    def launch_time(date, time)
      Launchy.open("http://www.moskva.fm/share/#{archive_code}/#{date.strftime('%Y%m%d')}/fromtime:#{time}:00")
    end

    def launch(options)
      if (options.empty?)
        launch_current
      else
        time = options.shift
        date = Date.today
        shift = options.shift
        date = date - shift.to_i if shift
        launch_time(date, time)
      end
    end

    def description
      "station #{name.to_s.humanize.green}"
    end

    private

    def site_url
      site == :moscowfm ? 'http://www.moskva.fm/' : 'http://www.piter.fm'
    end
end
