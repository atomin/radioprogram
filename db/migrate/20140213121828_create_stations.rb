class CreateStations < ActiveRecord::Migration
  def change
    create_table :stations do |t|
      t.string :name
      t.string :archive_code
      t.string :code
      t.string :site, default: 'moscowfm'

      t.timestamps
    end
  end
end
