class CreateRadioPrograms < ActiveRecord::Migration
  def change
    create_table :radio_programs do |t|
      t.references :station
      t.integer :day
      t.string :time
      t.string :name

      t.timestamps
    end
  end
end
