class CreateHeardPrograms < ActiveRecord::Migration
  def change
    create_table :heard_programs do |t|
      t.date :date
      t.references :radio_program

      t.timestamps
    end
  end
end
