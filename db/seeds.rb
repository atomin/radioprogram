russia = Station.find_or_create_by(name: 'russia', archive_code: '7873', code: 'FM_66.3', site: 'piter')
culture = Station.find_or_create_by(name: 'culture', archive_code: '2015', code: 'FM_91.6')

RadioProgram.find_or_create_by(name: 'wordless', station_id: russia.id, day: 2, time: '00:35')
RadioProgram.find_or_create_by(name: 'pastperfect', station_id: culture.id, day: 4, time: '00:00')
RadioProgram.find_or_create_by(name: 'exotica', station_id: culture.id, day: 6, time: '00:00')
RadioProgram.find_or_create_by(name: 'homemusic', station_id: culture.id, day: 6, time: '01:00')
